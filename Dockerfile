FROM python:3.8-slim-buster

WORKDIR /home
ENV TELEGRAM_API_TOKEN="111111111:AAAAAAAAAAAA"

RUN pip install -U pip aiohttp

COPY *.py ./
COPY *.db ./

ENTRYPOINT ["python", "main.py"]
