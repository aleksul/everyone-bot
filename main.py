#!/usr/bin/python3.8
import asyncio
import aiohttp

import tg_api
from bot_handler import BotHandler

import logging
import restart

from os import path, getenv
from random import shuffle

import sqlite3

folder = path.dirname(path.realpath(__file__))+'/'

logging.basicConfig(filename=f'{folder}bot.log',
                    format='%(asctime)s    %(levelname)s: %(message)s',
                    datefmt='%d/%m/%Y %H:%M:%S',
                    level=logging.INFO)
logging.info('Program started')
logging.debug(f'Current directory: {folder}')

db = sqlite3.connect('users.db')
sql = db.cursor()

ADMIN_ID = ['196846654']
ADMIN_COMMANDS = ['/admin', '/log', '/restart', '/clear_log', '/db']

RESTART_FLAG = 0
restart_str_list = ['Нет конечно!', 'Да, перезапуск!', 'Нет!', 'Неееет!']

BOT_TOKEN = "1189545299:AAEBzY-sAaiUKuDnUOJ2EXFX2PuEwYGvJTA"
#BOT_TOKEN = getenv("TELEGRAM_API_TOKEN")
BOT_NAME = 'everyone_tag_bot'
BOT_ID = BOT_TOKEN.split(':')[0]

bt_new_group = tg_api.InlineButtonBuilder('Добавить в группу', url=f"t.me/{BOT_NAME}?startgroup=init")
kb_new_group = tg_api.InlineMarkupBuilder([[bt_new_group]])

kb_admin = tg_api.KeyboardBuilder([['/log', '/restart'], ['/clear_log', '/db']],
                                  one_time_keyboard=False)


async def internet_check(site: str, timeout=3):
    try:
        async with aiohttp.request('GET',
                                   site, timeout=aiohttp.ClientTimeout(total=timeout)) as resp:
            assert resp.status == 200
            logging.debug(f"Internet seems to be connected. Response from {site}: {resp.status}")
    except Exception as error:
        logging.warning(f"Site {site} does not work: {type(error)}:{error}")
        return False
    else:
        return True


async def check_internet():
    results = await asyncio.gather(internet_check('http://example.org/'),
                                   internet_check(f'https://api.telegram.org/bot{BOT_TOKEN}/getMe'))
    if results == [False, False]:
        logging.critical('No internet connection at all!')
        raise restart.InternetConnectionError
    elif results == [True, False]:
        logging.error("Internet connection seems to be good, but telegram doesn't work!")
        raise restart.InternetConnectionError
    else:
        logging.debug("Got 200 OK from telegram server")
        return True


async def logic(bot):
    global RESTART_FLAG

    # getting update

    update = await bot.get_updates()
    if update is None:
        return None

    logging.debug(update)

    # parsing update
    text, command, admin_command, reply_to_message = False, False, False, False
    message_text, reply_text, data = '', '', ''

    if 'callback_query' in update.keys():
        return
    else:
        received_message = update['message']
        user_id = str(received_message['from']['id'])
        chat_id = str(received_message['chat']['id'])
        message_type = list(received_message.keys())[4]
        if message_type == 'text':
            message_text: str = received_message['text']
            text = True
            if message_text[0] == '/':
                command = True
                if chat_id == user_id and chat_id in ADMIN_ID and message_text in ADMIN_COMMANDS:
                    admin_command = True

    user_name = received_message['from']['first_name']
    if "username" in received_message['from'].keys():
        user_nick = "@"+received_message['from']['username']
    else:
        user_nick = user_name

    # working with commands

    if admin_command:
        if '/admin' == message_text:
            return asyncio.ensure_future(bot.send_message(user_id, f'Наконец то мой дорогой админ {user_name} '
                                                                   f'добрался до раздела админских возможностей!\n\n'
                                                                   f'• Напишите /log для получения файла логов,'
                                                                   f' /clear_log для того чтобы его отчистить\n'
                                                                   f'• Напишите /restart для '
                                                                   f'принудительной перезагрузки\n'
                                                                   f'• Напишите /db для получения файла базы данных',
                                                          reply_markup=kb_admin))
        elif '/log' == message_text:
            return asyncio.ensure_future(bot.send_file(user_id, f'{folder}bot.log', 'log.txt'))
        elif '/restart' == message_text:
            shuffle(restart_str_list)
            kb_restart = []
            for i in list(restart_str_list):
                kb_restart.append([i])
            RESTART_FLAG = 1
            return asyncio.ensure_future(bot.send_message(user_id, 'Вы уверены?',
                                                          reply_markup=tg_api.KeyboardBuilder(kb_restart)))
        elif '/clear_log' == message_text:
            with open(folder + 'bot.log', 'w'):  # log clearing
                pass
            logging.info('Cleared log')
            return asyncio.ensure_future(bot.send_message(user_id, 'Лог был отчищен!', reply_markup=kb_admin))
        elif '/db' == message_text:
            logging.info(f'Sending database to {chat_id}')
            return asyncio.ensure_future(bot.send_file(user_id, f'{folder}users.db', 'users.db'))
    elif command:
        if '/start' == message_text:
            return asyncio.ensure_future(bot.send_message(chat_id, f"Приветствую, {user_name}! \n"
                                                                   f"Я бот, который добавит команду "
                                                                   f"/everyone в твой чат!",
                                                          reply_markup=kb_new_group))
        elif '/everyone' == message_text or \
                f'/everyone@{BOT_NAME}' == message_text[0:len(f'/everyone@{BOT_NAME}')]:
            if chat_id == user_id:
                return asyncio.ensure_future(bot.send_message('Вы не в группе!'))
            sql.execute(f'SELECT User_Nick FROM {"group_id"+chat_id[1::]}')
            users_nicks = sql.fetchall()
            users_nicks = [i[0] for i in users_nicks]
            a = ''
            for i in users_nicks:
                a += i+' '
            users_nicks = a
            if users_nicks:
                return asyncio.ensure_future(bot.send_message(chat_id, f'{users_nicks}'))
            else:
                return asyncio.ensure_future(bot.send_message(chat_id, 'Вы никого не добавили!'))
        elif '/add' == message_text[0:4] or \
                f'/add@{BOT_NAME}' == message_text[0:len(f'/add@{BOT_NAME}')]:
            if chat_id == user_id:
                return asyncio.ensure_future(bot.send_message('Вы не в группе!'))
            member = await bot.get_chat_member(chat_id, user_id)
            if member['status'] == 'administrator' or member['status'] == 'creator':
                message_text: list = message_text.split(' ')
                if len(message_text) == 1:
                    return asyncio.ensure_future(bot.send_message(chat_id, 'Вы не указали имя пользователя'))
                message_text: str = message_text[1]
                if message_text.startswith("@"):
                    sql.execute(f'INSERT OR IGNORE INTO {"group_id"+chat_id[1::]}(User_Nick) VALUES(?)', [message_text])
                    db.commit()
                    return asyncio.ensure_future(bot.send_message(chat_id, f"Пользователь {message_text} "
                                                                           f"успешно добавлен!"))
                else:
                    return asyncio.ensure_future(bot.send_message(chat_id, f"Пользователь {message_text} НЕ добавлен!"))
            else:
                return asyncio.ensure_future(bot.send_message(chat_id, "Вы не администратор!"))
        elif '/del' == message_text[0:4] or \
                f'/del@{BOT_NAME}' == message_text[0:len(f'/del@{BOT_NAME}')]:
            if chat_id == user_id:
                return asyncio.ensure_future(bot.send_message('Вы не в группе!'))
            member = await bot.get_chat_member(chat_id, user_id)
            if member['status'] == 'administrator' or member['status'] == 'creator':
                message_text: list = message_text.split(' ')
                if len(message_text) == 1:
                    return asyncio.ensure_future(bot.send_message(chat_id, 'Вы не указали имя пользователя'))
                message_text: str = message_text[1]
                if not message_text:
                    return asyncio.ensure_future(bot.send_message(chat_id, 'Вы не указали имя пользователя'))
                sql.execute(f'DELETE FROM {"group_id"+chat_id[1::]} WHERE User_Nick = ?', [message_text])
                db.commit()
                return asyncio.ensure_future(bot.send_message(chat_id, f"Пользователь {message_text} успешно удален!"))
            else:
                return asyncio.ensure_future(bot.send_message(chat_id, "Вы не администратор!"))
        elif f'/start@{BOT_NAME} init' == message_text:
            sql.execute(f'CREATE TABLE IF NOT EXISTS {"group_id"+chat_id[1::]}(User_Nick TEXT, UNIQUE(User_Nick))')
            db.commit()
            return asyncio.ensure_future(bot.send_message(chat_id, 'Всем привет!'))
        elif '/help' == message_text or \
                f'/help@{BOT_NAME}' == message_text[0:len(f'/help@{BOT_NAME}')]:
            return asyncio.ensure_future(bot.send_message(chat_id, '• Добавь меня в группу \n'
                                                                   '• Напиши команду /everyone и я '
                                                                   'тэгну всех людей в чате \n'
                                                                   '• Команда доступна только админам группы',
                                                          reply_markup=kb_new_group)
                                         )

    elif text:
        if chat_id in ADMIN_ID and message_text == 'Да, перезапуск!' and RESTART_FLAG:
            RESTART_FLAG = 0
            await bot.send_message(user_id, 'Перезапускаюсь...', reply_markup=kb_admin)
            raise restart.UserRestart

        elif chat_id in ADMIN_ID and RESTART_FLAG:
            RESTART_FLAG = 0
            return asyncio.ensure_future(bot.send_message(user_id, 'Перезапуск отменен', reply_markup=kb_admin))


async def task_manager():
    async with aiohttp.ClientSession() as session:
        tg_bot = BotHandler(BOT_TOKEN, session)
        task_bot = asyncio.ensure_future(logic(tg_bot), loop=loop)
        while True:
            if task_bot.done():  # check if our task done
                task_bot.result()  # will raise error if task finished incorrectly
                task_bot = asyncio.ensure_future(logic(tg_bot), loop=loop)
            await asyncio.sleep(0)  # give control back to event loop


if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    task_check = loop.create_task(check_internet())
    try:
        proxy = loop.run_until_complete(task_check)
    except Exception as err:
        logging.critical(f'Restart caused: {type(err)}:{err}')
        loop.run_until_complete(asyncio.sleep(0.250))  # wait for all connections to close
        task_check.cancel()
        loop.stop()
        loop.close()
        asyncio.set_event_loop(asyncio.new_event_loop())
        sql.close()
        db.close()
        restart.program(folder + 'main.py', 10)
    else:
        task_main = loop.create_task(task_manager())
        logging.info("Start main program")
        try:
            loop.run_until_complete(task_main)
        except Exception as err:
            logging.critical(f'Restart caused: {type(err)}:{err}')
        finally:
            loop.run_until_complete(asyncio.sleep(0.250))  # wait for all connections to close
            task_main.cancel()
            loop.stop()
            loop.close()
            asyncio.set_event_loop(asyncio.new_event_loop())
            sql.close()
            db.close()
            restart.program(folder + 'main.py', 10)
else:
    logging.critical(f'__Name__ is NOT equal main! It is {__name__}')
